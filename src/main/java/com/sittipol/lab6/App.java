package com.sittipol.lab6;
/**
 * Hello world!
 *
 */

    public class App {
        public static void main(String[] args) {
            BookBank beer = new BookBank("Beer", 50.0); // Constuctor

            beer.print();
            BookBank mind = new BookBank("Mind", 100000.0);
            mind.print();
            mind.withdraw(40000.0);
            mind.print();

            beer.deposit(40000.0);
            beer.print();

            BookBank mhon = new BookBank("Mhon", 1000000.0);
            mhon.print();
            mhon.deposit(2);

    }
}

