package com.sittipol.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateSucess1(){
        Robot robot = new Robot("Robot",'R', 10,11);
        assertEquals("Robot", robot.getname());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }
    @Test
    public void shouldCreateSucees2(){
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getname());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    } 
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Rotbot", 'R', 0, Robot.Max_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.Max_Y, robot.getY());
    }

    @Test
    public void shouldUpNegative() {
        Robot robot = new Robot("Rotbot", 'R', 0, Robot.Min_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.Min_Y, robot.getY());
    }
    
    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }
    
    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldLeftSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 1, 1);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }   

    @Test
    public void shouldRightSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 0, 1);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }
    @Test
    public void shouldUpNSuccess1() {
        Robot robot = new Robot("Rotbot", 'R', 10, 11);
        boolean result =robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }
    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Rotbot", 'R', 10, 11);
        boolean result =robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldUpNFail1() {
        Robot robot = new Robot("Rotbot", 'R', 10, 11);
        boolean result =robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }
    @Test
    public void shouldDownNSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }
    @Test
    public void shouldLeftNSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 1, 1);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }
    @Test
    public void shouldRightNSuccess() {
        Robot robot = new Robot("Rotbot", 'R', 0, 1);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }
}

